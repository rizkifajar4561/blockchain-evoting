import React from "react";
import { Icon, IconGroup, Segment, SegmentGroup } from "semantic-ui-react";
import "../../App.css";

const Footer = () => {
  return (
    <Segment size="large" tertiary inverted color="blue" textAlign="center">
      Election DAPP @2022

    </Segment>
  );
};

export default Footer;
